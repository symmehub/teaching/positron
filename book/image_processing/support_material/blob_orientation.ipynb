{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Blob orientation and aspect ratio\n",
    "\n",
    "In this example, we show how to use inertia matrix of a given labeled object to find its orientation. \n",
    "\n",
    "![](blobs.jpg)\n",
    "\n",
    "```{note}\n",
    "You can right-click on the image to download it.\n",
    "```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# UNCOMMENT FOR INTERACTIVE PLOTS\n",
    "#%matplotlib notebook\n",
    "from PIL import Image\n",
    "import plotly.express as px\n",
    "import plotly.graph_objects as go\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy import ndimage\n",
    "from matplotlib import cm\n",
    "import pandas as pd\n",
    "import os\n",
    "import plotly.io as pio\n",
    "import plotly.offline as py_offline\n",
    "\n",
    "pio.renderers.default = \"png\"\n",
    "# plt.ion()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Image loading"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path = \"blobs.jpg\"\n",
    "files = os.listdir(\"./\")\n",
    "if path in files:\n",
    "    print(\"Ok, the file is in {0}\".format(files))\n",
    "else:\n",
    "    print(\"The file is not in {0} , retry !\".format(files))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to explain the concepts of inertia and aspect ratio, we use this magnificient hand drawing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "im = Image.open(path)\n",
    "fig = px.imshow(im)\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preprocessing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "R, G, B = im.split()\n",
    "R = np.array(R)\n",
    "G = np.array(G)\n",
    "B = np.array(B)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = {\"start\": 0, \"end\": 255, \"size\": 1}\n",
    "fig = go.Figure()\n",
    "fig.add_trace(go.Histogram(x=R.flatten(), xbins=bins, name=\"red\", marker_color=\"red\"))\n",
    "fig.add_trace(\n",
    "    go.Histogram(x=G.flatten(), xbins=bins, name=\"green\", marker_color=\"green\")\n",
    ")\n",
    "fig.add_trace(go.Histogram(x=B.flatten(), xbins=bins, name=\"blue\", marker_color=\"blue\"))\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thresholding level is obvious:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Bt = np.where(B < 50, 1, 0)\n",
    "\n",
    "fig = px.imshow(Bt, color_continuous_scale=\"gray\")\n",
    "fig.update_layout(coloraxis_showscale=False)\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Labelization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Btc = ndimage.binary_closing(Bt)\n",
    "\n",
    "Bl, number = ndimage.measurements.label(Btc)\n",
    "fig = px.imshow(\n",
    "    np.where(Bl != 0, Bl, np.nan), color_continuous_scale=px.colors.sequential.Jet\n",
    ")\n",
    "fig.update_layout(coloraxis_showscale=False)\n",
    "fig.show()\n",
    "\n",
    "f\"Found {number} blobs\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "obj = ndimage.measurements.find_objects(Bl)\n",
    "len(obj)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inertia matrix and orientation\n",
    "\n",
    "The object represented bellow is stretched in a direction. Let's see how we can use its inertia matrix to determine in which direction and how much it is stretched."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.imshow(np.array(im)[obj[1]])\n",
    "fig.update_layout(coloraxis_showscale=False)\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The inertia matrix of a 2D object can be defined as follows:\n",
    "\n",
    "$$\n",
    "I = \n",
    "\\begin{bmatrix} \n",
    "I_{xx} & -I_{xy} \\\\\n",
    "-I_{xy} & I_{yy} \\\\\n",
    "\\end{bmatrix}\n",
    "$$\n",
    "\n",
    "This matrix is symmetric and, as a consequence, it can be diagonalized in a new frame rotated by an angle $\\theta$ in the plane. This frame is composed of the two normalized eigenvectors $(\\vec e_1, \\vec e_2)$ of the matrix. In this frame, the matrix has two eigenvalues $(I_1, I_2)$ ordered so that $I_1 \\geq I_2$. Then: \n",
    "* $\\theta = (\\vec x, \\vec e_1)$ and,\n",
    "* The aspect ratio $a = \\sqrt{I_1 / I_2}$.\n",
    "\n",
    "The angle $\\theta$ gives the direction of the elongation of the object and $a$ shows how much it is elongated. For example,  if $a == 1$, the object is not elongated whereas if $a=10$ it is 10 times longer in direction 1 than in direction 2 in an inertial point of view."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = pd.DataFrame(\n",
    "    columns=[\"area\", \"xg\", \"yg\", \"Ixx\", \"Iyy\", \"Ixy\", \"I1\", \"I2\", \"theta\"]\n",
    ")\n",
    "for i in range(len(obj)):\n",
    "    x, y = np.where(Bl == i + 1)\n",
    "    xg, yg = x.mean(), y.mean()\n",
    "    x = x - xg\n",
    "    y = y - yg\n",
    "    A = len(x)\n",
    "    Ixx = (y ** 2).sum()\n",
    "    Iyy = (x ** 2).sum()\n",
    "    Ixy = (x * y).sum()\n",
    "    I = np.array([[Ixx, -Ixy], [-Ixy, Iyy]])\n",
    "    eigvals, eigvecs = np.linalg.eig(I)\n",
    "    eigvals = abs(eigvals)\n",
    "    loc = np.argsort(eigvals)[::-1]\n",
    "    d = eigvecs[loc[0]]\n",
    "    d *= np.sign(d[0])\n",
    "    theta = np.degrees(np.arccos(d[1]))\n",
    "    eigvals = eigvals[loc]\n",
    "    data.loc[i] = [A, xg, yg, Ixx, Iyy, Ixy, eigvals[0], eigvals[1], theta]\n",
    "data.sort_values(\"area\", inplace=True, ascending=False)\n",
    "data[\"aspect_ratio\"] = (data.I1 / data.I2) ** 0.5\n",
    "\n",
    "data[[\"area\", \"theta\", \"aspect_ratio\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from plotly.subplots import make_subplots\n",
    "\n",
    "fig = make_subplots(\n",
    "    rows=3, cols=4, subplot_titles=tuple([f\"{i}\" for i in data.index.values])\n",
    ")\n",
    "\n",
    "counter = 1\n",
    "col_id = 1\n",
    "row_id = 1\n",
    "for i in data.index.values:\n",
    "\n",
    "    z = Image.fromarray(np.array(im)[obj[i]])\n",
    "    z = z.rotate(-data.loc[i, \"theta\"] + 90, expand=True)\n",
    "    z = np.array(z)\n",
    "\n",
    "    fig.add_trace(go.Image(z=z), row=row_id, col=col_id)\n",
    "    fig.update_xaxes(showticklabels=False).update_yaxes(showticklabels=False)\n",
    "\n",
    "    col_id += 1\n",
    "    if col_id == 5:\n",
    "        col_id = 1\n",
    "        row_id += 1\n",
    "\n",
    "fig.update_layout(height=600, width=800)\n",
    "fig.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
