# Scientific Python

This package is a collection of Jupyter notebooks on various topics that are used to teach numerical analysis.

Authors:
* [Ludovic Charleux](mailto:ludovic.charleux@univ-smb.fr)
* [Emile Roux](mailto:emile.roux@univ-smb.fr)
* Thibault Goyallon
* Christian Elmo
* Giovanni Feverati
* Pierre Nagorny
* Camille Saint-Martin
